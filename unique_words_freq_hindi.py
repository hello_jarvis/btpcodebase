# -*- coding: utf-8 -*-
import codecs
import re,os
from nltk.stem import PorterStemmer, WordNetLemmatizer
from nltk.corpus import stopwords
from nltk.tokenize import sent_tokenize, word_tokenize
import string
from collections import defaultdict,OrderedDict

path='./nirala/poems'

vocabulary=[]
dictionary={}

for fn in os.listdir(path):
	filename = os.path.join(path, fn)
	file = codecs.open(filename,encoding='utf-8')
	text = file.read()
	file.close()
	text1=''
	for i in text:
		if i not in string.punctuation:
			text1+=i
		
	text=text1
	text=text[text.find('\n'):]
	sentences=[]
	sentences=text.split("\n")
	tokens=[]
	for s in sentences:
		words=s.split(' ')
		tokens=tokens+words




	


	suffixes = {
	    1: [u"ो",u"े",u"ू",u"ु",u"ी",u"ि",u"ा"],
	    2: [u"कर",u"ाओ",u"िए",u"ाई",u"ाए",u"ने",u"नी",u"ना",u"ते",u"ीं",u"ती",u"ता",u"ाँ",u"ां",u"ों",u"ें"],
	    3: [u"ाकर",u"ाइए",u"ाईं",u"ाया",u"ेगी",u"ेगा",u"ोगी",u"ोगे",u"ाने",u"ाना",u"ाते",u"ाती",u"ाता",u"तीं",u"ाओं",u"ाएं",u"ुओं",u"ुएं",u"ुआं"],
	    4: [u"ाएगी",u"ाएगा",u"ाओगी",u"ाओगे",u"एंगी",u"ेंगी",u"एंगे",u"ेंगे",u"ूंगी",u"ूंगा",u"ातीं",u"नाओं",u"नाएं",u"ताओं",u"ताएं",u"ियाँ",u"ियों",u"ियां"],
	    5: [u"ाएंगी",u"ाएंगे",u"ाऊंगी",u"ाऊंगा",u"ाइयाँ",u"ाइयों",u"ाइयां"],
	}

	for word in tokens:
		ans=word
		for L in 5, 4, 3, 2, 1:
				if len(word) > L + 1:
					for suf in suffixes[L]:
						if word.endswith(suf):
							ans=word[:-L]
							break
		
		if ans in dictionary:
			dictionary[ans]+=1
		else:
			dictionary[ans]=1
		vocabulary.append(ans)

for i in dictionary:
	print type(i), dictionary[i]
vocabulary=list(set(vocabulary))				
with open("./nirala/data/unique_words.txt",'w') as f1:
	for word in vocabulary:
		word=word.encode('utf-8')
		f1.write(str(word)+'\n')
with open("./nirala/data/word_frequency.txt",'w') as f1:	
	for key, value in dictionary.items():
		key=key.encode('utf-8')
		f1.write(str(key) + ' \t '+ str(value) + '\n\n')

