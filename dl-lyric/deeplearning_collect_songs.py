# g2p-seq2seq --decode /home/sakshi/BTPresult/TaylorSwift/data/unique_words.txt --model g2p-seq2seq-cmudict >> /home/sakshi/BTPresult/TaylorSwift//data/phonemes.txt
import os

old_artists=["JoniMitchell","MichaelJackson","Nirvana","RollingStones","JanisJoplin","Madonna","Metallica","PattiSmith","Queen","BobDylan"]
new_artists=["BebeRexha","JenniferLopez","BritneySpears","ArianaGrande","BrunoMars","SelenaGomez","ShawnMendes","JustinBieber","TaylorSwift"]
females=["JoniMitchell","JanisJoplin","Madonna","PattiSmith","BebeRexha","JenniferLopez","BritneySpears","ArianaGrande","SelenaGomez","TaylorSwift"]
males=["MichaelJackson","Nirvana","RollingStones","Metallica","Queen","BrunoMars","ShawnMendes","JustinBieber","BobDylan"]
path1 = "./deepdataset2/"
path2="./"
for a in old_artists:

	dir1=path1+"old_vs_new_2"
	dir3=dir1+"/old"
	dir2=path2+a+"/lyrics"
	if not os.path.exists(dir1):
			os.makedirs(dir1)
	if not os.path.exists(dir3):
			os.makedirs(dir3)
	for dir in os.listdir(dir2):
		dirname = os.path.join(dir2,dir)
		file = open(dirname+"/"+dir +".txt", 'rt')
		text = file.read()
		file.close()
		f=open(dir3+"/"+dir+".txt","wt")
		f.write(text)
		f.close()

for a in new_artists:

	dir1=path1+"old_vs_new_2"
	dir3=dir1+"/new"
	dir2=path2+a+"/lyrics"
	if not os.path.exists(dir1):
			os.makedirs(dir1)
	if not os.path.exists(dir3):
			os.makedirs(dir3)
	for dir in os.listdir(dir2):
		dirname = os.path.join(dir2,dir)
		file = open(dirname+"/"+dir +".txt", 'rt')
		text = file.read()
		file.close()
		f=open(dir3+"/"+dir+".txt","wt")
		f.write(text)
		f.close()

for a in males:

	dir1=path1+"male_vs_female_2"
	dir3=dir1+"/male"
	dir2=path2+a+"/lyrics"
	if not os.path.exists(dir1):
			os.makedirs(dir1)
	if not os.path.exists(dir3):
			os.makedirs(dir3)
	for dir in os.listdir(dir2):
		dirname = os.path.join(dir2,dir)
		file = open(dirname+"/"+dir +".txt", 'rt')
		text = file.read()
		file.close()
		f=open(dir3+"/"+dir+".txt","wt")
		f.write(text)
		f.close()

for a in females:

	dir1=path1+"male_vs_female_2"
	dir3=dir1+"/female"
	dir2=path2+a+"/lyrics"
	if not os.path.exists(dir1):
			os.makedirs(dir1)
	if not os.path.exists(dir3):
			os.makedirs(dir3)
	for dir in os.listdir(dir2):
		dirname = os.path.join(dir2,dir)
		file = open(dirname+"/"+dir +".txt", 'rt')
		text = file.read()
		file.close()
		f=open(dir3+"/"+dir+".txt","wt")
		f.write(text)
		f.close()
		