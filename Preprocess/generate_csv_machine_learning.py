import os

path = './BobDylan/lyrics'
resultpath='./BobDylan/data/csv_machine_learning.txt'
final={}
f=open(resultpath,'a')
for fn in os.listdir(path):
	phonemes=['AA','AE','AH','AO','AW','AY','B','CH','D','DH','EH','ER','EY','F','G','HH','IH','IY','JH','K','L','M','N','NG','OW','OY','P','R','S','SH','T','TH','UH','UW','V','W','Y','Z','ZH']
	phoneme_freq={}
	dirname = os.path.join(path, fn)
	file1 = open(dirname+'/'+'phoneme_distribution.txt', 'rt')
	file2 = open(dirname+'/'+'genre.txt', 'rt')
	phoneme_frequency_pairs = file1.readlines()
	genres=file2.readlines()
	file1.close()
	file2.close()
	genre=genres[0].strip()

	for l in range (len(phoneme_frequency_pairs)):
		pf=phoneme_frequency_pairs[l].strip().split('\t')
		phoneme=pf[0]
		freq=int(pf[1])
		phoneme_freq[phoneme]=freq
	for i in phonemes:
		if i not in phoneme_freq:
			phoneme_freq[i]=0	
	for fr in sorted(phoneme_freq):
		f.write(str(phoneme_freq[fr])+',')
	f.write(genre+ '\n')
