import os
from nltk.stem import PorterStemmer, WordNetLemmatizer
from nltk.corpus import stopwords
from nltk.tokenize import sent_tokenize, word_tokenize
import string
from collections import defaultdict,OrderedDict

path = './verse.txt'

path2='./verse_processed.txt'

with open(path, 'rt') as file:
	text = file.read()
	file.close()
	text = text.translate(None, ',')
	text = text.translate(None, '"')
	text = text.translate(None, '.')
	text = text.translate(None, '...')
	text = text.translate(None, '?')
	text = text.translate(None, '!')
	text = text.translate(None, ';')
	text = text.translate(None, '-')
	text = text.translate(None, '\'')
	text = text.translate(None, '.\'')
	text = text.translate(None, '(')
	text = text.translate(None, ')')
	text = text.translate(None, ':')
	text = text.translate(None, '>')
	text = text.translate(None, '<')
	tokens = word_tokenize(text)
	tokens = [w.lower() for w in tokens]

	words = [word for word in tokens if word.isalpha()]
	stop_words = set(stopwords.words('english'))
	words = [w for w in words if not w in stop_words]
	lemma=WordNetLemmatizer()
	lemmed=[lemma.lemmatize(word) for word in words]
        lemmed=set(lemmed)

	
with open(path2, 'a') as file:
	for word in lemmed:
		file.write(str(word)+ "\n")

