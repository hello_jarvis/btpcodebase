import os
from nltk.stem import PorterStemmer, WordNetLemmatizer
from nltk.corpus import stopwords
from nltk.tokenize import sent_tokenize, word_tokenize
import string
from collections import defaultdict,OrderedDict

path = './prose/written'
foo= './prose.txt'
c=0
vocabulary=[]
dictionary={}


with open(foo, 'a') as fo:

	for fn in os.listdir(path):
		filename = os.path.join(path, fn)
		file = open(filename, 'rt')
		text = file.read()
		file.close()
		fo.write(text)
		fo.write("\n")
fo.close()
