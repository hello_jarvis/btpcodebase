#change path, last line label

import os
f=open("bigram.csv","a")
path='./Beatles'
k=2

phoneme_list=['AH','AA','IH','IY','UH','UW','AY','EY','AE','EH','AO','AW','OW','OY','K','G','D','HH','CH','JH','Y','SH','T','R','ER','TH','DH','N','NG','L','S','Z','ZH','P','F','B','M','V','W']
all_features=set()
for i in phoneme_list:
	for j in phoneme_list:
		all_features.add(i+' '+j)
all_features=sorted(list(all_features))
# for i in all_features:
# 	f.write(i+',')
# f.write('Artist\n')

for fn in os.listdir(path):
	filename = os.path.join(path, fn)
	g=open(filename,"r")
	text=g.read()
	phonemes=text.strip().split()
	features={}
	for i in range(len(phonemes)-k):
		x=' '.join(phonemes[i:i+k])
		if x not in features:
			features[x]=0
		features[x]+=1
	for i in all_features:
		if i not in features:
			f.write(str(0)+',')
		else:
			f.write(str(features[i])+',')
	f.write('3\n')
