# g2p-seq2seq --decode /home/sakshi/BTPresult/Beatles/data/unique_words.txt --model g2p-seq2seq-cmudict >> /home/sakshi/BTPresult/Beatles/data/phonemes.txt
import os
phoneme_dict={}
f=open("./BobDylan/data/phonemes.txt")
lines=f.readlines()
for l in lines:
	i=l.strip().split()
	phoneme_dict[i[0]]=[]
	phoneme_dict[i[0]].extend(i[1:])

path = './BobDylan/data/'

phoneme_dist={}
file = open(path+'word_frequency.txt', 'rt')
text = file.readlines()
file.close()
for t in text:
	t=t.strip().split()
	word=t[0]
	freq=int(t[1])
	l=phoneme_dict[word]
	for i in l:
		if i not in phoneme_dist:
			phoneme_dist[i]=0
		phoneme_dist[i]+=freq
print phoneme_dist
f=open(path+'phoneme_distribution.txt','w')
for phoneme in sorted(phoneme_dist.keys()):
	f.write(phoneme+'\t'+str(phoneme_dist[phoneme])+'\n')
