import os
from nltk.stem import PorterStemmer, WordNetLemmatizer
from nltk.corpus import stopwords
from nltk.tokenize import sent_tokenize, word_tokenize
import string
from collections import defaultdict,OrderedDict
artists=["BebeRexha","JenniferLopez","BritneySpears","JoniMitchell"]
path1 = "./"
for a in artists:

	path = path1+a+'/lyrics'

	final_word_freq={}

	for fn in os.listdir(path):
		vocabulary=[]
		dictionary={}
		dirname = os.path.join(path, fn)
		file = open(dirname+'/'+fn+'.txt', 'rt')
		text = file.read()
		file.close()
		text = text.translate(None, ',')
		text = text.translate(None, '"')
		text = text.translate(None, '.')
		text = text.translate(None, '...')
		text = text.translate(None, '?')
		text = text.translate(None, '!')
		text = text.translate(None, ';')
		text = text.translate(None, '-')
		text = text.translate(None, '\'')
		text = text.translate(None, '.\'')
		text = text.translate(None, '(')
		text = text.translate(None, ')')
		text = text.translate(None, ':')
		text = text.translate(None, '>')
		text = text.translate(None, '<')
		tokens = word_tokenize(text)
		tokens = [w.lower() for w in tokens]

		words = [word for word in tokens if word.isalpha()]
		#stop_words = set(stopwords.words('english'))
		#words = [w for w in words if not w in stop_words]
		#lemma=WordNetLemmatizer()
		#lemmed=[lemma.lemmatize(word) for word in words]

		for w in words:
			if w not in dictionary:
				dictionary[w]=0
			if w not in final_word_freq:
				final_word_freq[w]=0
			dictionary[w]+=1
			final_word_freq[w]+=1
		                
		for w in words:
			if w not in vocabulary:
				vocabulary.append(w)

	# print final_word_freq
		with open(dirname+'/word_frequency.txt', 'w') as file:
			for key in sorted(dictionary.iterkeys()):
				file.write(str(key) +"\t\t"+ str(dictionary[key])+"\n")
		file.close()
		with open(dirname+'/unique_words.txt','w') as file:
			for key in sorted(vocabulary):
				file.write(str(key)+'\n')
